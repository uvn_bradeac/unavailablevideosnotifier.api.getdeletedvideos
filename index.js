const axios = require('axios')

exports.handler = async(event, context, callback) => {
    let getPlaylistIds = async(channelId) => {
        const getPlaylistsUrl = `${process.env.getPlaylistsUrl}${channelId}`

        const response = await axios.get(getPlaylistsUrl)
        
        if (response.data.errorMessage) {
            return response.data
        }

        return response.data.map(item => item.id)
    }

    let getDeletedTracks = async(playlistIds) => {
        let data = []
        const getTracksUrl = process.env.getTracksUrl

        await Promise.all(playlistIds.map(async(playlist) => {
            const response = await axios.get(getTracksUrl, {
                params: {
                    playlistid: playlist
                }
            })
            
            if (response.data.errorMessage) {
                return response.data
            }

            const deleted = response.data.filter(item => 
                item.title.toLowerCase().includes('private video') ||
                item.title.toLowerCase().includes('deleted video')
            )

            data = [...deleted, ...data]
        }))

        return data
    }

    try {
        const channelId = event.query.channelid

        const playlistIds = await getPlaylistIds(channelId)
        
        if (playlistIds.errorMessage) {
            callback(playlistIds.errorMessage, null)
        }
        
        const deletedTracksData = await getDeletedTracks(playlistIds)
        
    
        const result = deletedTracksData.map((track) => {
            return {
                id: track.id,
                playlistId: track.playlistId,
            }
        })
    
        callback(null, result)
    } catch(error) {
        callback(error, null)
    }
}